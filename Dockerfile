FROM alpine:3.8
RUN apk add --no-cache bash curl cifs-utils ca-certificates \
    && adduser -D -u 1000 chartmuseum

WORKDIR /opt

RUN wget https://s3.amazonaws.com/chartmuseum/release/latest/bin/linux/amd64/chartmuseum
RUN chmod +x /opt/chartmuseum

ADD entrypoint.sh /opt

ENTRYPOINT ["/opt/entrypoint.sh"]